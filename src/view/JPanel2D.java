/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JPanel;
import model.Model2D;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class JPanel2D extends JPanel implements Observer {

    int width, height;

    int size;

    int cols;

    private Model2D model;

    private int xdt;
    private int ydt;

    JPanel2D(Model2D model, int w, int h) {
        model.addObserver(this);
        //setSize(width = w, height = h);
        setPreferredSize(new Dimension(height = h,width = w));
        size = model.getSize();
        this.model = model;
        xdt = height / size;
        ydt = width / size;

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        width = getSize().width;
        height = getSize().height;
        xdt = height / size;
        ydt = width / size;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                Rectangle2D rect = new Rectangle(i * xdt, j * ydt, xdt, ydt);
                g2.setColor(model.getGrille()[j * size + i]==1 ? Color.CYAN : Color.BLACK);
                g2.fill(rect);
                g2.setColor(Color.RED);
                g2.draw(rect);
            }

        }
//        // pas super opti
//        for (int i = 0; i < size; i++) {
//            g2.setColor(Color.RED);
//            g2.drawLine(0, i * xdt, width, i * xdt);
//            g2.drawLine(i * ydt, 0, i * ydt, height);
//        }

    }

    @Override
    public void update(Observable o, Object arg) {
        repaint();
    }
}

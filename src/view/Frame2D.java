/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import model.Model;
import model.Model2D;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class Frame2D extends JFrame {

    final JButton n = new JButton("next");
    final JButton g = new JButton("go");
    final JButton s = new JButton("stop");
    final JButton r = new JButton("reset");
    final Timer timer;
    final Model model;

    public Frame2D(final Model2D model,int infvie,int supvie,int infnaissance,int supnaissance) throws IOException {
        super("Jeu de la vie vie : "+"["+infvie+","+supvie+"]"+" naissance : "+"["+infnaissance+","+supnaissance+"]");
        JPanel2D xyz = new JPanel2D(model, 640, 640);
        add(xyz);
        JPanel south = new JPanel(new GridLayout(1, 4));
        this.model = model;
        timer = new Timer(30, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                next();
            }
        });

        n.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                next();
            }
        });

        r.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });

        g.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                go();
            }
        });

        s.setEnabled(false);
        s.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stop();
            }
        });

        south.add(r);
        south.add(n);
        south.add(g);
        south.add(s);
        add(south, BorderLayout.SOUTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    public void stop() {
        n.setEnabled(true);
        g.setEnabled(true);
        timer.stop();
    }

    public void next() {
        if (model.getRuns() < model.getIterationsMax()) {
            model.computeAtomique();
        } else {
            stop();
            maxiteration();
        }
    }

    public void reset() {
        n.setEnabled(true);
        s.setEnabled(false);
        g.setEnabled(true);
        timer.stop();
        model.reset();
    }

    public void go() {
        g.setEnabled(false);
        n.setEnabled(false);
        s.setEnabled(true);
        timer.start();
    }

    public void maxiteration() {
        g.setEnabled(false);
        n.setEnabled(false);
        s.setEnabled(false);
    }

}

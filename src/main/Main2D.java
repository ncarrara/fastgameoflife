package main;

import java.awt.List;
import java.io.IOException;
import java.util.ArrayList;
import model.Model2D;
import view.Frame2D;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class Main2D {

    public static void main(String[] args) throws IOException {

        if (false) {
            ArrayList<int[]> configs = new ArrayList<>();

            for (int i = 0; i < 9; i++) {
                for (int j = i; j < 9; j++) {
                    int[] config = {i, j};
                    configs.add(config);
                }

            }

            System.out.println("size : " + configs.size());
            for (int i = 0; i < configs.size(); i++) {
                for (int j = 0; j < configs.size(); j++) {
                    System.out.println("" + configs.get(i)[0] + " " + configs.get(i)[1] + " " + configs.get(j)[0] + " " + configs.get(j)[1]);
                    Model2D m = new Model2D(50, configs.get(i)[0], configs.get(i)[1], configs.get(j)[0], configs.get(j)[1]);
                    //new Frame2D(m, configs.get(i)[0], configs.get(i)[1], configs.get(j)[0], configs.get(j)[1]);
                }

            }
        } else {
            Model2D m;
            //converge très vite vers une configuration avec très peu de cellule
//            m = new Model2D(50, 2, 2, 3, 3);
//            new Frame2D(m, 2, 2, 3, 3);
            
            m = new Model2D(50, 2, 3, 3, 3);
            new Frame2D(m, 2, 3, 3, 3);
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class Model1D extends Model {

    private int[] ruban;
    
    private double random;

    public Model1D(int taille, double random){
        super();
        ruban = new int[taille];
        randomise(random);
        this.random = random;
    }
    
    
    public Model1D(int[] ruban){
        this.ruban = ruban;
    }
    
    @Override
    protected void areset() {
//        ruban = new int[ruban.length];
//        randomise(random);
    }

    @Override
    protected int f(int etat, int voisinesVivantes) {
        int etatSuivant = 0;
        if ((etat == 1 && (voisinesVivantes == 0 )) || ((etat == 0) && (voisinesVivantes == 1))) {
            etatSuivant = 1;
        }
        return etatSuivant;
    }

    @Override
    protected int voisinesVivantes(int position) {
//        System.out.println("position = "+position);
        int voisins = 0;
        // plus rapide que des modulos imo
        int avant = position == 0 ? ruban.length - 1 : position - 1;
        int apres = position == (ruban.length - 1) ? 0 : position + 1;
//        System.out.println("avant : "+avant);
//        System.out.println("apres : "+apres);
        voisins = ruban[avant] + ruban[apres];
        return voisins;
    }

    @Override
    public int[] getGrille() {
        return ruban;
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < ruban.length; i++) {
            s += ruban[i] == 0 ? " " : "X";
        }

        return s+"    "+getRuns();
    }

}

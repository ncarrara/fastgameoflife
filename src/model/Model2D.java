/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class Model2D extends Model{

    private int size;
    
    private int[] grille;
    
    private int infvie = 2;
    
    private int supvie = 3;
    
    private int infnaissance = 3;
    
    private int supnaissance  = 3;
    
    public Model2D(int[] initialisation) {
        super();
        grille = initialisation; 
        size = (int) Math.pow(initialisation.length, 0.5);
    }
    
    public Model2D(int taille){
        super();
        grille = new int[taille * taille];
        randomise(0.7);
        size = taille;
    }
    
    public Model2D(int taille,int infvie,int supvie,int infnaissance,int supnaissance){
        this(taille);
        this.infvie = infvie;
        this.supvie = supvie;
        this.infnaissance = infnaissance;
        this.supnaissance = supnaissance;
    }
    
    @Override
    protected void areset() {
        grille = new int[getGrille().length];
        randomise(0.7);
    }
    
    // abstract
    @Override
    protected int f(int etat, int voisinesVivantes) {
        int etatSuivant = 0;
        if (((etat == 0) && (voisinesVivantes >= infnaissance && voisinesVivantes <= supnaissance))
                || ((etat == 1) && (voisinesVivantes >= infvie && voisinesVivantes <= supvie))) {
            etatSuivant = 1;
        }
        return etatSuivant;
    }

    // abstract
    @Override
    protected int voisinesVivantes(int position) {
        // todo a optimiser
        int i = position / getSize();
        int j = position % getSize();
        int io = (i - 1) < 0 ? getSize() - 1 : i - 1;
        int ie = (i + 1) % getSize();
        int jn = (j - 1) < 0 ? getSize() - 1 : (j - 1);
        int js = (j + 1) % getSize();
        
        int voisins = 
                getGrille()[io * getSize() + j] 
                + getGrille()[ie * getSize() + j] 
                + getGrille()[i * getSize() + jn] 
                + getGrille()[i * getSize() + js]
                + getGrille()[io * getSize() + jn]
                + getGrille()[ie * getSize() + js]
                + getGrille()[ie * getSize() + jn]
                + getGrille()[io * getSize() + js];
        return voisins;
    }
    
       /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int[] getGrille() {
        return grille;
    }


    
    
}

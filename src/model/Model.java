/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Arrays;
import java.util.Observable;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public abstract class Model extends Observable {

    private int[] grilleSuivante;

    private int runs;

    private int ITERATION_MAX = Integer.MAX_VALUE;

    public void randomise(double bernouilli) {
        for (int i = 0; i < getGrille().length; i++) {
            getGrille()[i] = Math.random() > bernouilli ? 1 : 0;
        }
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getIterationsMax() {
        return ITERATION_MAX;
    }

    public void update() {
        setChanged();
        notifyObservers();
    }

    public void reset() {
        areset();
        setRuns(0);
        update();
    }

    public abstract int[] getGrille();

    protected abstract void areset();

    protected abstract int f(int etat, int voisinesVivantes);

    protected abstract int voisinesVivantes(int position);

    public void computeAtomique() {
        if (runs == 0) {
            grilleSuivante = Arrays.copyOf(getGrille(), getGrille().length);
        }
        for (int i = 0; i < getGrille().length; i++) {
            getGrilleSuivante()[i] = f(getGrille()[i], voisinesVivantes(i));
        }
        System.arraycopy(getGrilleSuivante(), 0, getGrille(), 0, getGrille().length);
        runs++;
        update();
    }

    /**
     * @return the grilleSuivante
     */
    private int[] getGrilleSuivante() {
        return grilleSuivante;
    }

    /**
     * @param grilleSuivante the grilleSuivante to set
     */
    private void setGrilleSuivante(int[] grilleSuivante) {
        this.grilleSuivante = grilleSuivante;
    }

}
